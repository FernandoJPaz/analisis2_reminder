import { Component, OnInit } from '@angular/core';

import { TaskService } from '../services/task.service';
import { NavController, AlertController  } from '@ionic/angular';
import { MenuController, LoadingController } from '@ionic/angular';

import { UserOptions } from '../interfaces/task';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  home: UserOptions = { correo1: '', passwd1: '' };
  arrayPosts:any; //Creamos la variable donde guardaremos los datos que nos retorna el servicio
  load: any;

  constructor(public taskService: TaskService, 
    public loadingCtrl: LoadingController, public alertCtrl:AlertController,
    public router: Router) { }

  ngOnInit() {
  }

  ionViewDidLoad() {
    this.getPosts();//Llamamos a la función getPost cuando la vista se cargue
  }

  getPosts() { //llamamos a la funcion getPost de nuestro servicio.
    this.taskService.getPosts()
    .then(data => {
      this.arrayPosts = data;
    });
  }

  async login(){

    
    
    this.taskService.getLogin({ "correo": this.home.correo1, "passwd": this.home.passwd1 })
    
    .subscribe(async json => {   
    
      this.arrayPosts = json;

      
      this.load = await this.loadingCtrl.create({
        spinner: 'lines',
        showBackdrop: true,
        message: "Espere por favor...",
        translucent: true,
        cssClass: 'ion-loading',
        duration: 30000
      });

      if(json[0].correo != undefined){

        if(this.home.correo1 == json[0].correo && this.home.passwd1 ==json[0].passwd){
          console.log("Login Valido");
          this.router.navigateByUrl('/recordatorio');
        }

      }else{
        console.log("ERROR");
        this.router.navigateByUrl('/login');
      
      }
    });
  }

  doAlert(){
    
  }

}
