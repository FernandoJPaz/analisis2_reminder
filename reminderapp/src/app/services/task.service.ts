import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  

  data: any;
  url= 'http://localhost:5000'
  constructor(public http: HttpClient) { }

  getPosts(){
    return new Promise(resolve=>{
      this.http.get(this.url).subscribe(data=>{
          resolve(data);
      },error=>{
        console.log(error);
      });
    });
  }

  postUser(data) {

    
    const path = `${this.url}/CrearUsuario`;
    console.log(data);
    console.log(path);
    return this.http.post(path,data);
  }
  

  getLogin(data) {
    const path = `${this.url}/Iniciar`
    //console.log("-------");
    //console.log(data);
    return this.http.post(path, data);
  }

}
