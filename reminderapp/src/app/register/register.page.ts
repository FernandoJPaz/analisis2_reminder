import { Component, OnInit } from '@angular/core';

import { TaskService } from '../services/task.service';

import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  
  mail:string;
  nombre:string;
  password:string;
  
  submitted = false;


  constructor(public taskService: TaskService, public router: Router) { }

  ngOnInit() {
  }

  registro(){

    this.submitted = true;
    let data = JSON.stringify(
      {
        "correo": this.mail,
        "nombre": this.nombre,
        "passwd": this.password,
      });


      const task = {
        

        "correo" : this.mail,
        "nombre": this.nombre,
        "passwd": this.password,

      };

      this.router.navigateByUrl('/login');
      this.taskService.postUser(task)
      .subscribe((newTask) => {
        console.log(newTask);  
      });

    
  }

}
